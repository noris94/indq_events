const express= require('express');
const router=express.Router();

const {showEvents, addEvent, attendEvent, noAttendEvent} = require('../controllers/events');

router.route('/') 
.get(showEvents)
.post(addEvent);

router.route('/attendance/:eventId') 
.post(attendEvent)
.delete(noAttendEvent);


module.exports= router;