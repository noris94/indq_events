const pool=require('../database/pool_connection');
const bcrypt= require('bcrypt');
const path = require('path');

module.exports={ 
    "registerUser": (req,resp)=>{  
        bcrypt.hash(req.body.password,10) //encripta la contraseña
        .then(function (hash){
            //si no hay error en la encriptacion, entonces se procedera a guardar la info en la db
            var datos = [ 
                req.body.firstName,
                req.body.lastName,
                req.body.email,
                hash,
                req.body.gender
                ];
            return datos;
            }
        )
        .then(async function (datos){
            try {
                const client = await pool.connect();
                const email = await client.query("SELECT email FROM users WHERE email=$1",[req.body.email]);
                if (email.rows[0]!=null){
                    resp.status(403).json({ Descripcion: "La cuenta con ese correo electrónico ya existe"});
                }else{
                    const {max} = await client.query("SELECT MAX(id) FROM users");
                    await client.query('INSERT INTO users ("firstName","lastName",email,password,gender) values ($1,$2,$3,$4,$5)',datos)
                    resp.status(200).json({"id": `${max+1}`});
                }
                client.release();
            } 
            catch (error) {
                resp.status(500).json({
                    error: err.message
                });
            }  
        })
        .catch(function (err){
            resp.status(500).json({ //si no se encripta bien, mandara un 500 Error Interno de Servidor
                error: err.stack
                });
            }
        );   
    },
    "login": (req,res)=>{
        
    },

    "getForm":(req, res)=>{
        res.render('users/register');
    }
};