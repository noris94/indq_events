const express= require('express');
const router=express.Router();

const {addImage,getImage} = require('../controllers/images');

router.route('/')
.post(addImage);

router.route('/:fileName') 
.get(getImage);


module.exports= router;