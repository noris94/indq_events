const morgan=require('morgan');
const bodyParser =require('body-parser');
const express = require('express');
const exphbs=require('express-handlebars');
const path =require('path');
const app =express();

const usersRoutes =  require('./routes/users');
const eventsRoutes = require('./routes/events');
const imagesRoutes = require('./routes/images');

//settings
app.set('json spaces', 4); //configura los json con espaciado de 4
app.set('views',path.join(__dirname, 'views'));//une el nombre del directorio de este app.js y le agrega /views
/* configurare el motor de visualizacion con las caracteristicas de handlebars*/
app.engine('.hbs', exphbs({
    defaultLayout: 'main', //es el layout default y esta dentro de la carpeta views/layouts
    layoutsDir: path.join(app.get('views'),'layouts'), //esta buscando la propiedad que inventamos en la linea 14 que se llama views que contiene el directorio .../src/views y le agrega /layouts
    partialsDir: path.join(app.get('views'),'partials'),
    extname: '.hbs',
    helpers: require('./lib/handlebars')
}));
app.set('view engine','.hbs');

//middlewares
app.use(morgan('dev'));//me muestra en la consola los mensajes de los verbos HTTP
app.use(bodyParser.json());//para poder manejar json en el body de las peticiones y respuestas
app.use(bodyParser.urlencoded({extended:false}));

//routes
app.use('/users',usersRoutes);
app.use('/events',eventsRoutes);
app.use('/images',imagesRoutes);






module.exports = app;// vamos a exportar la const app al server.js llamandola como require('ubciacion de archivo');