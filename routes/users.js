const express= require('express');
const router=express.Router();

const {registerUser,login,getForm} = require('../controllers/users');

router.route('/') 
.post(registerUser)
.get(getForm);

router.route('/login') 
.post(login);


module.exports= router;