CREATE DATABASE "INDQ_DB"
    WITH 
    OWNER = noris
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;

--creacion de tabla usuarios--

CREATE TABLE public.users
(
    id bigserial NOT NULL,
    "firstName" character varying(35) NOT NULL,
    "lastName" character varying(35) NOT NULL,
    email character varying(45) NOT NULL,
    password text NOT NULL,
    gender character varying(8) NOT NULL,
    PRIMARY KEY (id)
);

--creacion de tabla eventos
CREATE TABLE public.eventos
(
    id bigserial NOT NULL,
    title character varying(100) NOT NULL,
    description text NOT NULL,
    date text NOT NULL,
    image text NOT NULL,
    attendances bigint,
    "willYouAttend" boolean,
    location double precision,
    PRIMARY KEY (id)
);





